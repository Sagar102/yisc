var typed = new Typed('#typed', {
    stringsElement: '#typed-string',
    typeSpeed: 200,
    backSpeed: 20,
    smartBackspace: true,
    loop: true,
    loopCount: Infinity,
    showCursor: false,
    cursorChar: '|',
    autoInsertCss: true,
    contentType: 'html',
  });
  

  