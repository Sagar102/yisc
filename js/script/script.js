$(document).ready(function () {

    $(document).scroll(function (e) {
        $(window).scrollTop() > 500 ? $('.topbar-section').addClass('test') : $('.topbar-section').removeClass('test');

        if ($(this).scrollTop() > 200) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });

        // =========================ScrollSpy=============================
        $('body').scrollspy({
            target: '#navbarSupportedContent',

        });
    });


});